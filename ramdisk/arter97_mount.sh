#!/sbin/busybox sh

/sbin/busybox chown root:root /arter97/data/arter97_secondrom/system
/sbin/busybox chown system:system /arter97/data/arter97_secondrom/data
/sbin/busybox chown system:cache /arter97/data/arter97_secondrom/cache

/sbin/busybox chmod 755 /arter97/data/arter97_secondrom/system
/sbin/busybox chown 771 /arter97/data/arter97_secondrom/data
/sbin/busybox chown 770 /arter97/data/arter97_secondrom/cache

/sbin/busybox mount -t ext4 -o noatime,nosuid,nodev,journal_async_commit,errors=panic /dev/block/mmcblk0p3 /efs
/sbin/busybox mount -t ext4 -o noatime,nodiratime,data=writeback,barrier=0,discard,nosuid,nodev,discard,noauto_da_alloc,journal_async_commit,errors=panic /dev/block/mmcblk0p12 /arter97/data
/sbin/busybox mount -t f2fs -o noatime,nodiratime,discard,nosuid,nodev /dev/block/mmcblk0p12 /arter97/data

/sbin/busybox mount --bind /arter97/data/arter97_secondrom/system /system
/sbin/busybox mount --bind -o ro,remount,suid /system
/sbin/busybox mount --bind /arter97/data/arter97_secondrom/data /data
/sbin/busybox mount --bind /arter97/data/arter97_secondrom/cache /cache

if [ -f /arter97/data/media/0/.arter97/shared ]; then
	/sbin/busybox mount --bind /arter97/data/media/0 /data/media
fi

export PATH=/sbin:/system/sbin:/system/bin:/system/xbin
/sbin/busybox run-parts /arter97/data/arter97_secondrom/init.d

/sbin/busybox sync
